[_tb_system_call storage=system/_scene1.ks]

[cm  ]
[bg  storage="街道/廧戭奨乮擔拞乯.jpg"  time="1000"  ]
[tb_show_message_window  ]
[playbgm  volume="50"  time="1000"  loop="true"  storage="Dive_into_the_Carpet.ogg"  ]
[tb_start_text mode=1 ]
入学前......[p]
[_tb_end_text]

[bg  time="3000"  method="crossfade"  storage="部屋/堦恖晹壆乮擔拞乯乥墱儗僀儎.jpg"  ]
[chara_show  name="夏川龍也"  time="1000"  wait="true"  storage="chara/1/24-1月-14-09-09-33-13.png"  width="985"  height="776"  left="157"  top="52"  reflect="false"  ]
[tb_start_text mode=1 ]
#
俺は夏川龍也、年齢18歳。[p]
#
中学時代に天才と呼ばれ、飛び級で大学に進学した。[p]
#母
龍也、朝食の準備ができました。[p]
#
分かりました、母親。[p]
[_tb_end_text]

[chara_hide  name="夏川龍也"  time="1000"  wait="true"  pos_mode="true"  ]
[bg  time="1000"  method="crossfade"  storage="部屋/儕價儞僌乮擔拞乯.jpg"  ]
[tb_start_text mode=1 ]
#皆
いただきます。[p]
#龍也
おいしい！[p]
#龍也
ごちそうさまでした、いってきます。[p]
#母
いってらっしゃい。[p]
#
[_tb_end_text]

[stopbgm  time="1000"  ]
[bg  time="1000"  method="crossfade"  storage="街道/廧戭奨乮擔拞乯.jpg"  ]
[chara_show  name="夏川龍也"  time="1000"  wait="true"  storage="chara/1/24-1月-14-09-09-33-13.png"  width="761"  height="599"  left="194"  top="138"  reflect="false"  ]
[playse  volume="50"  time="1000"  buf="0"  storage="1/ボクシングの打撃音.ogg"  ]
[chara_mod  name="夏川龍也"  time="600"  cross="true"  storage="chara/1/24-1月-14-09-09-34-00.png"  ]
[bg  time="1000"  method="fadeIn"  storage="0.jpg"  cross="true"  ]
[chara_hide  name="夏川龍也"  time="20"  wait="true"  pos_mode="true"  ]
[glink  color="white"  storage="yes.ks"  size="20"  text="目を開けて"  target="*yes"  x="532"  y="304"  width="200"  height=""  _clickable_img=""  ]
[glink  color="white"  storage="no.ks"  size="20"  text="目を閉じて"  target="*no"  x="532"  y="428"  width="200"  height=""  _clickable_img=""  ]
[s  ]
