[_tb_system_call storage=system/_no.ks]

*no

[mask_off  time="1000"  effect="fadeOut"  ]
[bg  time="1000"  method="crossfade"  storage="昦堾偺儘價乕丒庴晅乮徠柧OFF乯.jpg"  ]
[chara_show  name="夏川龍也"  time="1000"  wait="true"  storage="chara/1/24-1月-14-09-09-34-24.png"  width="842"  height="659"  left="485"  top="79"  reflect="false"  ]
[playse  volume="50"  time="1000"  buf="0"  storage="不穏な気配.mp3"  ]
[stopbgm  time="1000"  ]
[tb_start_text mode=1 ]
#
ここは…どこ。[p]
#謎の人
病院。[p]
#
誰？[p]
#謎の人
すみません、自己紹介を忘れてました。[p]
#
私の名前は凧。[p]
[_tb_end_text]

[chara_mod  name="夏川龍也"  time="600"  cross="true"  storage="chara/1/24-1月-14-09-09-33-13.png"  ]
[chara_show  name="凧"  time="1000"  wait="true"  storage="chara/2/24-1月-15-12-12-14-11.png"  width="843"  height="663"  left="-31"  top="75"  reflect="false"  ]
[tb_start_text mode=1 ]
#凧
初めまして。[p]
#
私は[p]
[_tb_end_text]

[chara_mod  name="凧"  time="600"  cross="true"  storage="chara/2/24-2月-04-03-03-43-44.png"  ]
[tb_start_text mode=1 ]
#凧
龍也、夏川龍也ですね。[p]
中学時代に天才と呼ばれ、飛び級で大学に進学した。[p]
父は原因不明の事故で亡くなり、母に育てられました。[p]
#
なぜ私の情報を知っているのですか。[p]
#凧
伝えられなくてごめんなさい。[p]
でも約束できるよ、私は悪い人じゃない。[p]
#
では、なぜ私は病院にいる​​のでしょうか？[p]
#凧
私はその通りを通りかかったとき、[p]
あなたが地面に横たわっているのを見ました。[p]
原因不明[p]
あなたが内臓に損傷を負ったのではないかと心配だったので、病院に送りました。[p]
#
それはそれで、分かりました。[p]
今、大丈夫です。[p]
お先に家に帰ります。[p]
[_tb_end_text]

[chara_mod  name="凧"  time="600"  cross="true"  storage="chara/2/24-1月-15-12-12-14-11.png"  ]
[tb_start_text mode=1 ]
#凧
あの......龍也さん、お腹が空いたみたいです。[p]
そして、私は住む場所がないようです。[p]
#
それなら私の家に泊まりに行ってください。[p]
大丈夫、ゲストルームはありますのでご安心ください。[p]
#凧
ありがとうございます。[p]
[_tb_end_text]

[chara_hide_all  time="1000"  wait="true"  ]
[bg  time="1000"  method="crossfade"  storage="街道/廧戭奨乮栭丒摂傝柍偟乯.jpg"  ]
[bg  time="1000"  method="crossfade"  storage="部屋/儕價儞僌乮栭丒徠柧OFF丒僇乕僥儞暵乯.jpg"  ]
[bg  time="1000"  method="crossfade"  storage="部屋/堦恖晹壆乮栭丒寧柧傝乯乥墱儗僀儎.jpg"  ]
[tb_start_text mode=1 ]
#凧
龍也さん、あなたの机の上に封筒があります。[p]
ラブレターですか？[p]
#
ありえない。[p]
見てみましょう。[p]
[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="0.jpg"  ]
[tb_start_text mode=1 ]
「龍也の家族に代々伝わる遺伝子が、世界を危機に晒す大切な役割を果たす者として選ばれたことが書かれていた」[p]
『ウェスティン』マフィアという落款があります。[p]
[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="部屋/堦恖晹壆乮栭丒寧柧傝乯乥墱儗僀儎.jpg"  ]
[tb_start_text mode=1 ]
#
『ウェスティン』マフィア？私の家族？[p]
それを聞いたことはない。[p]
#凧
おお！ 今覚えています。[p]
#
どうしたの？[p]
#凧
家族が私に、あなたを訓練するためにここに来るように頼んだのです。[p]
#
ええええええええええええ！！[p]
[_tb_end_text]

[s  ]
