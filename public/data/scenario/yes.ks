[_tb_system_call storage=system/_yes.ks]

*yes

[playse  volume="50"  time="1000"  buf="0"  storage="キラキラ_-_ウィンドチャイム.mp3"  ]
[bg  time="1000"  method="crossfade"  storage="奨拞偺棤捠傝乮梉曽乯.jpg"  ]
[tb_start_text mode=1 ]
#謎の人
ボールが当たっただけですか。[p]
#
誰の声。[p]
[_tb_end_text]

[chara_show  name="昼間渡瑠"  time="1000"  wait="true"  storage="chara/3/24-2月-04-01-01-11-50.png"  width="542"  height="427"  left="91"  top="258"  reflect="false"  ]
[chara_move  name="昼間渡瑠"  anim="true"  time="300"  effect="linear"  wait="true"  left="237"  top="102"  width="794"  height="624"  ]
[chara_mod  name="昼間渡瑠"  time="800"  cross="true"  storage="chara/3/24-2月-04-01-01-12-02.png"  ]
[tb_start_text mode=1 ]
#謎の人
ボールが当たってしまって本当にごめんなさい。[p]
#
大丈夫、大丈夫、ただの鼻血です。[p]
[_tb_end_text]

[chara_mod  name="昼間渡瑠"  time="600"  cross="true"  storage="chara/3/24-2月-04-01-01-34-00.png"  ]
[tb_start_text mode=1 ]
#謎の人
えっ！本当に大丈夫ですか？[p]
#
大丈夫、よく傷つくのには慣れてる。[p]
ところで、あなたの名前は何ですか？[p]
#謎の人
名前？[p]
ああああああああ！自己紹介を忘れていました。[p]
私は昼間瑠雅です。瑠雅と呼んでいいよ。[p]
#
瑠雅...瑠雅、とても懐かしい名前ですね。[p]
初めまして、私は夏川龍也。[p]
#昼間瑠雅
ええええ‼[p]
龍也君！お久しぶりですもう8年も10年も会っていない。[p]
#
ええええ！[p]
[_tb_end_text]

[chara_hide  name="昼間渡瑠"  time="1000"  wait="true"  pos_mode="true"  ]
[bg  time="1000"  method="crossfade"  storage="title.jpg"  ]
[tb_start_text mode=1 ]
#昼間瑠雅
それは私が幼稚園の頃で、隅っこであなたを見つけて挨拶しましたが。[p]
他の人たちは陰であなたのことを悪く言っていました。それは私が幼稚園の頃でした。[p]
とても腹が立ったので、彼らにレッスンを教えに行きました......[p]
[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="奨拞偺棤捠傝乮梉曽乯.jpg"  ]
[chara_show  name="昼間渡瑠"  time="1000"  wait="true"  storage="chara/3/24-2月-04-01-01-12-39.png"  width="872"  height="687"  left="194"  top="78"  reflect="false"  ]
[tb_start_text mode=1 ]
#
その時、陰で私の悪口を言う人たちに教訓を教えるのを手伝ってくれた人がいたのは事実です。[p]
でも、その人の名前は昼間渡瑠......[p]
#昼間瑠雅
それは私の以前の名前でした......[p]
#
ええええ！なぜこの名前に変更されたのでしょうか？[p]
#昼間瑠雅
信じられないかもしれないけど本当だよ。[p]
両親が男の子の名前に似ていると思ったからです。[p]
だから名前を変えたんです。[p]
#
分かりました......[p]
じゃ......君とちゃんどちらがいいですか。[p]
#昼間瑠雅
瑠雅、瑠雅でいい。[p]
#
ああああ！入学儀式遅くなりそう[p]
バイバイ、瑠雅。[p]
#昼間瑠雅
バイバイ。[p]
[_tb_end_text]

[chara_hide  name="昼間渡瑠"  time="1000"  wait="true"  pos_mode="true"  ]
[bg  time="1000"  method="crossfade"  storage="0.jpg"  ]
[tb_start_text mode=1 ]
#
式典終了後、生徒たちが続々と教室に戻ってきました。[p]
[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="教室/嫵幒偺慜偺曽乮梉曽乯.jpg"  ]
[tb_start_text mode=1 ]
#先生
これが新しい転校生です、自己紹介をしてください。[p]
#
私は夏川龍也、年齢18歳。[p]
趣味は歌うとバスケットボール。[p]
初めましてどうぞよろしくお願いいたします。[p]
#先生
座席はクラスメイトの隣に座るだけです。[p]
#
[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="0.jpg"  ]
[tb_start_text mode=1 ]
放課後[p]
[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="街道/廧戭奨乮栭丒摂傝桳傝乯.jpg"  ]
[bg  time="1000"  method="crossfade"  storage="部屋/儕價儞僌乮栭丒徠柧OFF丒僇乕僥儞暵乯.jpg"  ]
[bg  time="1000"  method="crossfade"  storage="部屋/堦恖晹壆乮栭丒寧柧傝乯乥墱儗僀儎.jpg"  ]
[tb_start_text mode=1 ]
#
なんでテーブルの上に封筒があるの？[p]
[_tb_end_text]

[glink  color="white"  storage=""  size="20"  x="591"  y="303"  width="50"  height="30"  text="開ける"  _clickable_img=""  target="*封筒を開ける"  ]
[glink  color="white"  storage=""  size="20"  x="591"  y="412"  width="50"  height="30"  text="開けない"  _clickable_img=""  target="*封筒を開けない"  ]
[s  ]
*封筒を開ける

[bg  time="1000"  method="crossfade"  storage="0.jpg"  ]
[tb_start_text mode=1 ]
「龍也の家族に代々伝わる遺伝子が、世界を危機に晒す大切な役割を果たす者として選ばれたことが書かれていた」『ウェスティン』マフィアという落款があります。[p]
[_tb_end_text]

[s  ]
*封筒を開けない

[tb_start_text mode=1 ]
#
忘れてください、もう見ないでください、今日は本当に疲れています。[p]
[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="0.jpg"  ]
[tb_start_text mode=1 ]
達成：平凡な一日[p]
[_tb_end_text]

[s  ]
